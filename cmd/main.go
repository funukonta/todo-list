package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	_ "todo-list/docs"
	"todo-list/internal/routes"
	"todo-list/pkg"

	_ "github.com/joho/godotenv/autoload"
	httpSwagger "github.com/swaggo/http-swagger/v2"
)

// @title Todo List API
// @version 1.0
// @description This is Postman replacement for API Documentation

// @contact.name Evan Roy @ Dev
// @contact.email evanroy36@gmail.com

// @BasePath /

//	@tag.name			Posts
//	@tag.name			Tag

func main() {
	db, err := pkg.ConnectPostgre()
	if err != nil {
		log.Fatal("Connection failed:", err.Error())
	}

	mux := http.NewServeMux()

	mux.HandleFunc("GET /", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/swagger/index.html", 301)
	})

	mux.Handle("GET /swagger/", httpSwagger.Handler(
		httpSwagger.URL("/swagger/doc.json"),                        // The url pointing to API definition
		httpSwagger.DefaultModelsExpandDepth(httpSwagger.HideModel), // Models will not be expanded
	))

	routes.Routes(mux, db)

	port := fmt.Sprintf(":%s", os.Getenv("PORT"))

	log.Println("API Server listening to port", port)
	http.ListenAndServe(port, mux)
}
