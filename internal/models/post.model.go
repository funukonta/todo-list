package models

type Post struct {
	ID          int    `json:"id" db:"id"`
	Title       string `json:"title" db:"title"`
	Content     string `json:"content" db:"content"`
	Status      string `json:"status" db:"status"`
	PublishDate string `json:"publish_date" db:"publish_date"`
	Tags        []Tag  `json:"tags" db:"-"`
}

type PostReq struct {
	Title   string   `json:"title" db:"title" example:"Mencuci Piring"`
	Content string   `json:"content" db:"content" example:"Mencuci piring dirumah nenek"`
	Tags    []string `json:"tags" db:"-" example:"membantu,tugas"`
}
