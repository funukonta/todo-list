package pkg

import (
	"encoding/json"
	"net/http"
)

func JsonBody(r *http.Request, v any) error {
	err := json.NewDecoder(r.Body).Decode(v)
	defer r.Body.Close()
	return err
}

type JsonResponse struct {
	Code    int `json:"code"`
	Message any `json:"msg"`
	Data    any `json:"data,omitempty"`
}

func Response(code int, msg any, data any) *JsonResponse {
	var res JsonResponse
	res.Code = code

	if msg != nil {
		if _, ok := msg.(error); ok {
			res.Message = msg.(error).Error()
		} else {
			res.Message = msg
		}
	}

	if data != nil {
		res.Data = data
	}

	return &res
}

func (j *JsonResponse) Send(w http.ResponseWriter) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(j.Code)

	json.NewEncoder(w).Encode(j)
}
