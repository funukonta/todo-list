# Todo List App

Todo List App - developed with golang std lib.

## Installation

using docker compose
```bash
$ git clone https://gitlab.com/funukonta/todo-list.git
$ cd todo-list/
$ docker compose up -d
```

## Test the endpoints
Simply go to http://localhost:8000 to open API documentation.