# Stage 1: Build the Go binary
FROM golang:1.22.2-alpine

WORKDIR /todo-list

COPY . .

# Download dependencies
RUN go mod download

#build
RUN go build -v -o /todo-list/todo ./cmd/main.go

#run app
ENTRYPOINT [ "/todo-list/todo" ]