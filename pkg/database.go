package pkg

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/lib/pq"
)

func ConnectPostgre() (*sql.DB, error) {
	host := os.Getenv("DB_HOST")
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASS")
	dbname := os.Getenv("DB_NAME")
	ssl := os.Getenv("DB_SSL")

	connStr := fmt.Sprintf("postgresql://%s:%s@%s/%s?sslmode=%s", user, password, host, dbname, ssl)

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Panicln("error connStr", err.Error())
	}

	err = db.Ping()
	if err != nil {
		log.Panicln("error ping", err.Error())
	}

	err = migration(db)
	if err != nil {
		return nil, fmt.Errorf("Migrasi db fail, %s", err.Error())
	}

	return db, nil
}

func migration(db *sql.DB) error {
	query := `
	
	-- Create posts table
	CREATE TABLE if not exists posts (
		id SERIAL PRIMARY KEY,
		title VARCHAR(255) NOT NULL,
		content TEXT NOT NULL,
		status VARCHAR(10) CHECK (status IN ('draft', 'publish')) NOT NULL,
		publish_date TIMESTAMP
	);

	-- Create tags table
	CREATE TABLE if not exists  tags (
		id SERIAL PRIMARY KEY,
		label VARCHAR(50) UNIQUE NOT NULL
	);

	-- Create post_tags table to handle many-to-many relationship
	CREATE TABLE if not exists post_tags (
		post_id INT REFERENCES posts(id) ON DELETE CASCADE,
		tag_id INT REFERENCES tags(id) ON DELETE CASCADE,
		PRIMARY KEY (post_id, tag_id)
	);
	`

	_, err := db.Exec(query)
	return err
}
