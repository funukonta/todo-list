package services

import (
	"fmt"
	"todo-list/internal/models"
	"todo-list/internal/repositories"
)

type PostService interface {
	Insert(*models.PostReq) error
	GetAll() ([]models.Post, error)
	GetPostByTag(string) ([]models.Post, error)
	Update(*models.PostReq, int) error
	Delete(id int) error
	GetOne(id int) (*models.Post, error)
	Reset() error
}

type postService struct {
	repo repositories.PostRepo
}

func New_PostService(repo repositories.PostRepo) PostService {
	return &postService{repo: repo}
}

func (s *postService) Insert(postReq *models.PostReq) error {
	post := &models.Post{
		Title:   postReq.Title,
		Content: postReq.Content,
	}
	post.Status = "draft"
	post.PublishDate = "2000-01-01" // means not publish, default value

	for _, tag := range postReq.Tags {
		post.Tags = append(post.Tags, models.Tag{Label: tag})
	}

	err := s.repo.Insert(post)
	if err != nil {
		return err
	}

	return nil
}

func (s *postService) GetAll() ([]models.Post, error) {
	posts, err := s.repo.GetAll()
	if err != nil {
		return nil, err
	}

	if len(posts) == 0 {
		return nil, fmt.Errorf("Tidak ada Post")
	}

	return posts, err
}

func (s *postService) GetPostByTag(tag string) ([]models.Post, error) {
	posts, err := s.repo.GetPostByTag(tag)
	if err != nil {
		return nil, err
	}

	if len(posts) == 0 {
		return nil, fmt.Errorf("Tidak ada Post")
	}

	return posts, err
}

func (s *postService) Update(postReq *models.PostReq, id int) error {
	post := models.Post{
		ID:      id,
		Title:   postReq.Title,
		Content: postReq.Content,
	}

	post.Status = "draft"
	post.PublishDate = "2000-01-01" // means not publish, default value

	for _, tag := range postReq.Tags {
		post.Tags = append(post.Tags, models.Tag{Label: tag})
	}

	err := s.repo.Update(&post)
	if err != nil {
		return err
	}

	return nil
}

func (s *postService) Delete(id int) error {
	return s.repo.Delete(id)

}

func (s *postService) GetOne(id int) (*models.Post, error) {
	return s.repo.GetOne(id)
}

func (s *postService) Reset() error {
	err := s.repo.Reset()
	return err
}
