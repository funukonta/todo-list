package routes

import (
	"database/sql"
	"net/http"
	"todo-list/internal/handlers"
	"todo-list/internal/repositories"
	"todo-list/internal/services"
)

func PostsRoute(m *http.ServeMux, db *sql.DB) {
	repo := repositories.New_PostRepo(db)
	service := services.New_PostService(repo)
	postHandler := handlers.New_PostHandler(service)

	m.HandleFunc("POST /api/posts", serveApi(postHandler.Insert))
	m.HandleFunc("GET /api/posts", serveApi(postHandler.GetAll))
	m.HandleFunc("GET /api/posts/{id_post}", serveApi(postHandler.GetOne))
	m.HandleFunc("PUT /api/posts/{id_post}", serveApi(postHandler.Update))
	m.HandleFunc("DELETE /api/posts/{id_post}", serveApi(postHandler.Delete))

	m.HandleFunc("GET /api/reset", serveApi(postHandler.Reset))
}
