package repositories

import (
	"database/sql"
	"todo-list/internal/models"
)

type TagRepo interface {
	Insert(*models.Tag) error
	GetAll() ([]models.Tag, error)
	GetOne(*models.Tag) error
	Update(*models.Tag) error
	Delete(*models.Tag) error
}

type tagRepo struct {
	*sql.DB
}

func New_TagRepo(db *sql.DB) TagRepo {
	return &tagRepo{db}
}

func (r *tagRepo) Insert(tag *models.Tag) error {
	query := `INSERT into tags (label) values ($2); returning id`
	err := r.DB.QueryRow(query, tag.Label).Scan(tag.ID)

	return err
}

func (r *tagRepo) GetAll() ([]models.Tag, error) {
	query := `select * from tags`
	rows, err := r.DB.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	tags := []models.Tag{}
	for rows.Next() {
		tag := models.Tag{}
		err := rows.Scan(&tag.ID, &tag.Label)
		if err != nil {
			return nil, err
		}
		tags = append(tags, tag)
	}

	return tags, err
}

func (r *tagRepo) GetOne(tag *models.Tag) error {
	query := `select label from tags where id = $1`
	err := r.DB.QueryRow(query, tag.ID).Scan(&tag.Label)
	return err
}

func (r *tagRepo) Update(tag *models.Tag) error {
	query := `UPDATE tags set label = $1 where id = $2;`
	_, err := r.DB.Exec(query, tag.Label, tag.ID)
	return err
}

func (r *tagRepo) Delete(tag *models.Tag) error {
	query := `DELETE from tags where id = $1`
	err := r.DB.QueryRow(query, tag.ID).Scan(&tag.Label)
	return err
}
