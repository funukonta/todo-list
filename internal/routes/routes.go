package routes

import (
	"database/sql"
	"net/http"
	"todo-list/pkg"
)

func Routes(m *http.ServeMux, db *sql.DB) {
	PostsRoute(m, db)
	TagsRoute(m, db)
}

type apiHandler func(w http.ResponseWriter, r *http.Request) error

func serveApi(f apiHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := f(w, r)
		if err != nil {
			pkg.Response(http.StatusBadRequest, err, nil).Send(w)
		}
	}
}
