package repositories

import (
	"database/sql"
	"fmt"
	"time"
	"todo-list/internal/models"
)

type PostRepo interface {
	Insert(*models.Post) error
	GetAll() ([]models.Post, error)
	GetPostByTag(string) ([]models.Post, error)
	Update(*models.Post) error
	Delete(id int) error
	GetOne(id int) (*models.Post, error)
	Reset() error
}

type postRepo struct {
	*sql.DB
}

func New_PostRepo(db *sql.DB) PostRepo {
	return &postRepo{db}
}

func (r *postRepo) Insert(post *models.Post) error {
	tx, err := r.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	query := `INSERT into posts (title, content, status, publish_date) VALUES ($1, $2, $3, $4) 
	RETURNING id`
	err = tx.QueryRow(query, post.Title, post.Content, post.Status, post.PublishDate).Scan(&post.ID)
	if err != nil {
		return err
	}

	for _, tag := range post.Tags {
		var tagID int
		err = tx.QueryRow(`SELECT id FROM tags WHERE label = $1`, tag.Label).Scan(&tagID)
		if err == sql.ErrNoRows {
			err = tx.QueryRow(`INSERT INTO tags (label) VALUES ($1) RETURNING id`, tag.Label).Scan(&tagID)
		}
		if err != nil {
			return err
		}
		_, err = tx.Exec(`INSERT INTO post_tags (post_id, tag_id) VALUES ($1, $2)`, &post.ID, tagID)
		if err != nil {
			return err
		}
	}

	err = tx.Commit()

	return err
}

func (r *postRepo) GetAll() ([]models.Post, error) {
	query := `select * from posts;`
	rows, err := r.Query(query)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, fmt.Errorf("Tidak ada Post")
		}
		return nil, err
	}
	defer rows.Close()

	posts := []models.Post{}
	for rows.Next() {
		post := models.Post{}
		rows.Scan(&post.ID,
			&post.Title,
			&post.Content,
			&post.Status,
			&post.PublishDate)
		rowsTag, err := r.DB.Query(`SELECT t.label FROM tags t 
                           JOIN post_tags pt ON t.id = pt.tag_id 
                           WHERE pt.post_id = $1`, post.ID)

		if err != nil {
			return nil, fmt.Errorf("%s - id post: %d", err.Error(), post.ID)
		}
		defer rowsTag.Close()

		for rowsTag.Next() {
			var tag models.Tag
			err := rowsTag.Scan(&tag.Label)
			if err != nil {
				return nil, fmt.Errorf("tag scan, %s - id post: %d", err.Error(), post.ID)
			}
			post.Tags = append(post.Tags, tag)
		}

		posts = append(posts, post)
	}

	return posts, nil
}

func (r *postRepo) GetPostByTag(tag string) ([]models.Post, error) {
	query := `SELECT p.* from posts p
	left join post_tags pt on p.ID = pt.post_id
	left join tags t on t.ID = pt.tag_id where t.label = $1`
	rows, err := r.DB.Query(query, tag)
	if err != nil {
		return nil, err
	}
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, fmt.Errorf("Tidak ada Post")
		}
		return nil, err
	}
	defer rows.Close()

	posts := []models.Post{}
	for rows.Next() {
		post := models.Post{}
		rows.Scan(&post.ID,
			&post.Title,
			&post.Content,
			&post.Status,
			&post.PublishDate)
		rowsTag, err := r.DB.Query(`SELECT t.label FROM tags t 
                           JOIN post_tags pt ON t.id = pt.tag_id 
                           WHERE pt.post_id = $1`, post.ID)

		if err != nil {
			return nil, fmt.Errorf("%s - id post: %d", err.Error(), post.ID)
		}
		defer rowsTag.Close()

		for rowsTag.Next() {
			var tag models.Tag
			err := rowsTag.Scan(&tag.Label)
			if err != nil {
				return nil, fmt.Errorf("tag scan, %s - id post: %d", err.Error(), post.ID)
			}
			post.Tags = append(post.Tags, tag)
		}

		posts = append(posts, post)
	}

	return posts, err
}

func (r *postRepo) Update(post *models.Post) error {
	tx, err := r.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	publishDate, err := time.Parse("2006-01-02", post.PublishDate)
	if err != nil {
		return err
	}

	aff, err := tx.Exec(`UPDATE posts SET title = $1, content = $2, status = $3, publish_date = $4 WHERE id = $5`,
		post.Title, post.Content, post.Status, publishDate, post.ID)
	if err != nil {
		return err
	}

	rowAff, err := aff.RowsAffected()
	if err != nil {
		return err
	}
	if rowAff == 0 {
		return fmt.Errorf("Gagal Update, Post tidak ditemukan")
	}

	// var totalTags int
	// query := "select count(post_id) from post_tags where post_id = $1"
	// err = tx.QueryRow(query, post.ID).Scan(&totalTags)
	// if err != nil {
	// 	return err
	// }

	// PERFORM DELETE post tags
	_, err = tx.Exec(`DELETE FROM post_tags where post_id = $1 `, post.ID)
	if err != nil {
		return err
	}

	for _, tag := range post.Tags {
		var tagID int
		err = tx.QueryRow(`SELECT id FROM tags WHERE label = $1`, tag.Label).Scan(&tagID)
		if err == sql.ErrNoRows {
			err = tx.QueryRow(`INSERT INTO tags (label) VALUES ($1) RETURNING id`, tag.Label).Scan(&tagID)
		}
		if err != nil {
			return err
		}

		_, err = tx.Exec(`INSERT INTO post_tags (post_id,tag_id) values ($1,$2);`, post.ID, tagID)
		if err != nil {
			return err
		}
	}

	err = tx.Commit()

	return err
}

func (r *postRepo) Delete(id int) error {

	query := `DELETE posts where ID = $1`
	_, err := r.DB.Exec(query, id)
	if err != nil {
		return err
	}

	return nil
}

func (r *postRepo) GetOne(id int) (*models.Post, error) {
	post := &models.Post{}
	query := `select * from posts where ID = $1;`
	err := r.QueryRow(query, id).Scan(&post.ID,
		&post.Title,
		&post.Content,
		&post.Status,
		&post.PublishDate)
	if err == sql.ErrNoRows {
		return nil, fmt.Errorf("Tidak ditemukan")
	}
	if err != nil {
		return nil, err
	}

	rowsTag, err := r.DB.Query(`SELECT t.label FROM tags t 
                           JOIN post_tags pt ON t.id = pt.tag_id 
                           WHERE pt.post_id = $1`, post.ID)

	if err != nil {
		return nil, fmt.Errorf("%s - id post: %d", err.Error(), post.ID)
	}
	defer rowsTag.Close()

	for rowsTag.Next() {
		var tag models.Tag
		err := rowsTag.Scan(&tag.Label)
		if err != nil {
			return nil, fmt.Errorf("tag scan, %s - id post: %d", err.Error(), post.ID)
		}
		post.Tags = append(post.Tags, tag)
	}

	return post, nil
}

func (r *postRepo) Reset() error {
	query := `
	DROP TABLE post_tags;
	DROP TABLE posts;
	DROP TABLE tags;

	-- Create posts table
	CREATE TABLE if not exists posts (
		id SERIAL PRIMARY KEY,
		title VARCHAR(255) NOT NULL,
		content TEXT NOT NULL,
		status VARCHAR(10) CHECK (status IN ('draft', 'publish')) NOT NULL,
		publish_date TIMESTAMP
	);

	-- Create tags table
	CREATE TABLE if not exists  tags (
		id SERIAL PRIMARY KEY,
		label VARCHAR(50) UNIQUE NOT NULL
	);

	-- Create post_tags table to handle many-to-many relationship
	CREATE TABLE if not exists post_tags (
		post_id INT REFERENCES posts(id) ON DELETE CASCADE,
		tag_id INT REFERENCES tags(id) ON DELETE CASCADE,
		PRIMARY KEY (post_id, tag_id)
	);
	`

	_, err := r.DB.Exec(query)
	if err != nil {
		return err
	}

	return nil
}
