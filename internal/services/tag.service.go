package services

import (
	"fmt"
	"todo-list/internal/models"
	"todo-list/internal/repositories"
)

type TagService interface {
	Insert(*models.Tag) error
	GetAll() ([]models.Tag, error)
	GetOne(int) (*models.Tag, error)
	Update(int, *models.Tag) error
	Delete(int) error
}

type tagService struct {
	repo repositories.TagRepo
}

func New_TagRepo(repo repositories.TagRepo) TagService {
	return &tagService{repo: repo}
}

func (s *tagService) Insert(tag *models.Tag) error {
	return s.repo.Insert(tag)
}

func (s *tagService) GetAll() ([]models.Tag, error) {
	tags, err := s.repo.GetAll()
	if err != nil {
		return nil, err
	}

	if len(tags) == 0 {
		return nil, fmt.Errorf("Tidak ada tags")
	}

	return tags, err
}

func (s *tagService) GetOne(id int) (*models.Tag, error) {
	tag := &models.Tag{
		ID: id,
	}
	err := s.repo.GetOne(tag)
	return tag, err
}

func (s *tagService) Update(id int, tag *models.Tag) error {
	tag.ID = id
	err := s.repo.Update(tag)
	return err
}

func (s *tagService) Delete(id int) error {
	tag := &models.Tag{
		ID: id,
	}
	err := s.repo.Delete(tag)
	return err
}
