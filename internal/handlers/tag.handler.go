package handlers

import (
	"fmt"
	"net/http"
	"strconv"
	"todo-list/internal/models"
	"todo-list/internal/services"
	"todo-list/pkg"
)

type TagHandler interface {
	Insert(w http.ResponseWriter, r *http.Request) error
	GetAll(w http.ResponseWriter, r *http.Request) error
	GetOne(w http.ResponseWriter, r *http.Request) error
	Update(w http.ResponseWriter, r *http.Request) error
	Delete(w http.ResponseWriter, r *http.Request) error
}

type tagHandler struct {
	serv services.TagService
}

func New_TagHandler(serv services.TagService) TagHandler {
	return &tagHandler{serv: serv}
}

// @Tags		Tag
// @Summary		Create a tag
// @Param		Request	body	models.Tag	 true "Example Request"
// @Accept 		json
// @Produce 	json
// @Router		/api/tags [post]
func (h *tagHandler) Insert(w http.ResponseWriter, r *http.Request) error {
	tag := models.Tag{}
	err := pkg.JsonBody(r, &tag)
	if err != nil {
		return err
	}

	err = h.serv.Insert(&tag)
	if err != nil {
		return err
	}

	pkg.Response(http.StatusOK, "Berhasil insert tag", tag).Send(w)
	return nil
}

// @Tags		Tag
// @Summary		Get all tag in DB
// @Accept 		json
// @Produce 	json
// @Router		/api/tags [get]
func (h *tagHandler) GetAll(w http.ResponseWriter, r *http.Request) error {
	tags, err := h.serv.GetAll()
	if err != nil {
		return err
	}

	pkg.Response(http.StatusOK, "Berhasl ambil tags", tags).Send(w)
	return nil
}

func (h *tagHandler) GetOne(w http.ResponseWriter, r *http.Request) error {
	id, err := strconv.Atoi(r.PathValue("id_tag"))
	if err != nil {
		return fmt.Errorf("ID tag tidak valid")
	}

	tag, err := h.serv.GetOne(id)
	if err != nil {
		return err
	}

	pkg.Response(http.StatusOK, "Berhasl ambil tag", tag).Send(w)
	return nil
}

// @Tags		Tag
// @Summary		Update a tag
// @Param		Request	body	models.Tag	 true "Example Request"
// @Param 		id_tag path int true "ID of tag"
// @Accept 		json
// @Produce 	json
// @Router		/api/tag/{id_tag} [put]
func (h *tagHandler) Update(w http.ResponseWriter, r *http.Request) error {
	id, err := strconv.Atoi(r.PathValue("id_tag"))
	if err != nil {
		return fmt.Errorf("ID tag tidak valid")
	}

	tag := models.Tag{}
	err = pkg.JsonBody(r, &tag)
	if err != nil {
		return err
	}

	err = h.serv.Update(id, &tag)
	if err != nil {
		return err
	}

	pkg.Response(http.StatusOK, "Berhasl update tag", tag).Send(w)
	return nil
}

// @Tags		Tag
// @Summary		Delete a tag
// @Param 		id_tag path int true "ID of tag"
// @Accept 		json
// @Produce 	json
// @Router		/api/tag/{id_tag} [delete]
func (h *tagHandler) Delete(w http.ResponseWriter, r *http.Request) error {
	id, err := strconv.Atoi(r.PathValue("id_tag"))
	if err != nil {
		return fmt.Errorf("ID tag tidak valid")
	}

	err = h.serv.Delete(id)
	if err != nil {
		return err
	}

	pkg.Response(http.StatusOK, fmt.Sprintf("Berhasl hapus tag : %d", id), nil).Send(w)
	return nil
}
