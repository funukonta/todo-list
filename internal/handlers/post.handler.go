package handlers

import (
	"fmt"
	"net/http"
	"strconv"
	"todo-list/internal/models"
	"todo-list/internal/services"
	"todo-list/pkg"
)

type PostHandler interface {
	Insert(w http.ResponseWriter, r *http.Request) error
	GetAll(w http.ResponseWriter, r *http.Request) error
	Update(w http.ResponseWriter, r *http.Request) error
	Delete(w http.ResponseWriter, r *http.Request) error
	GetOne(w http.ResponseWriter, r *http.Request) error
	Reset(w http.ResponseWriter, r *http.Request) error
}

type postHandler struct {
	serv services.PostService
}

func New_PostHandler(serv services.PostService) PostHandler {
	return &postHandler{serv: serv}
}

// @Tags		Posts
// @Summary		Create a post
// @Param		Request	body	models.PostReq	 true "Example Request"
// @Accept 		json
// @Produce 	json
// @Router		/api/posts [post]
func (h *postHandler) Insert(w http.ResponseWriter, r *http.Request) error {
	var post models.PostReq
	err := pkg.JsonBody(r, &post)
	if err != nil {
		return err
	}

	err = h.serv.Insert(&post)
	if err != nil {
		return err
	}

	pkg.Response(http.StatusOK, "Berhasil create posts", post).Send(w)

	return nil
}

// @Tags		Posts
// @Summary		Get all post in database
// @Accept 		json
// @Produce 	json
// @Param 		tags query string false "query param of label tags"
// @Router		/api/posts [get]
func (h *postHandler) GetAll(w http.ResponseWriter, r *http.Request) error {
	var posts []models.Post
	var err error

	tag := r.URL.Query().Get("tag")
	if tag != "" {
		posts, err = h.serv.GetPostByTag(tag)
	} else {
		posts, err = h.serv.GetAll()
	}
	if err != nil {
		return err
	}

	pkg.Response(http.StatusOK, "Berhasil ambil posts", posts).Send(w)

	return nil
}

// @Tags		Posts
// @Summary		Update a post
// @Param		Request	body	models.PostReq	 true "Example Request"
// @Param 		id_post path int true "ID of post"
// @Accept 		json
// @Produce 	json
// @Router		/api/posts/{id_post} [put]
func (h *postHandler) Update(w http.ResponseWriter, r *http.Request) error {
	var post models.PostReq
	var err error
	id, err := strconv.Atoi(r.PathValue("id_post"))
	if err != nil {
		pkg.Response(http.StatusBadRequest, fmt.Sprintf("Bad Request, %s", err.Error()), nil).Send(w)
		return err
	}

	err = pkg.JsonBody(r, &post)
	if err != nil {
		pkg.Response(http.StatusBadRequest, err, nil).Send(w)
		return err
	}

	err = h.serv.Update(&post, id)
	if err != nil {
		pkg.Response(http.StatusBadRequest, err, nil).Send(w)
		return err
	}

	pkg.Response(http.StatusOK, "Berhasil update posts", post).Send(w)

	return nil
}

// @Tags		Posts
// @Summary		Delete a post
// @Accept 		json
// @Produce 	json
// @Param 		id_post path int true "ID of post"
// @Router		/api/posts/{id_post} [delete]
func (h *postHandler) Delete(w http.ResponseWriter, r *http.Request) error {
	id, err := strconv.Atoi(r.PathValue("id_post"))
	if err != nil {
		pkg.Response(http.StatusBadRequest, "Bad Request", nil).Send(w)
		return err
	}

	err = h.serv.Delete(id)
	if err != nil {
		pkg.Response(http.StatusBadRequest, err, nil).Send(w)
		return err
	}

	pkg.Response(http.StatusOK, fmt.Sprintf("Berhasil delete post : %d", id), nil).Send(w)

	return nil
}

// @Tags		Posts
// @Summary		Get a single Post
// @Accept 		json
// @Produce 	json
// @Param 		id_post path int true "ID of post"
// @Router		/api/posts/{id_post} [get]
func (h *postHandler) GetOne(w http.ResponseWriter, r *http.Request) error {
	id, err := strconv.Atoi(r.PathValue("id_post"))
	if err != nil {
		pkg.Response(http.StatusBadRequest, err, nil).Send(w)
		return err
	}

	posts, err := h.serv.GetOne(id)
	if err != nil {
		pkg.Response(http.StatusBadRequest, err, nil).Send(w)
		return err
	}

	pkg.Response(http.StatusOK, fmt.Sprintf("Berhasil ambil post : %d", id), posts).Send(w)

	return nil
}

func (h *postHandler) Reset(w http.ResponseWriter, r *http.Request) error {
	err := h.serv.Reset()
	if err != nil {
		return err
	}

	pkg.Response(http.StatusOK, "Reset", nil).Send(w)
	return nil
}
