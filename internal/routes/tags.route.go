package routes

import (
	"database/sql"
	"net/http"
	"todo-list/internal/handlers"
	"todo-list/internal/repositories"
	"todo-list/internal/services"
)

func TagsRoute(m *http.ServeMux, db *sql.DB) {
	repo := repositories.New_TagRepo(db)
	serv := services.New_TagRepo(repo)
	tagHandler := handlers.New_TagHandler(serv)

	m.HandleFunc("POST /api/tag", serveApi(tagHandler.Insert))
	m.HandleFunc("GET /api/tag", serveApi(tagHandler.GetAll))
	m.HandleFunc("GET /api/tag/{id_tag}", serveApi(tagHandler.GetOne))
	m.HandleFunc("PUT /api/tag/{id_tag}", serveApi(tagHandler.Update))
	m.HandleFunc("DELETE /api/tag/{id_tag}", serveApi(tagHandler.Delete))

}
