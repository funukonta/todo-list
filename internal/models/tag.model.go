package models

type Tag struct {
	ID    int    `json:"id,omitempty" db:"id"`
	Label string `json:"label" db:"label" example:"olahraga"`
}
